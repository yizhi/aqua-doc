##########################
Parameters
##########################

Each step in the pipeline involves several parameters. Most of them do not
need to be changed.

Data parameters
================

.. csv-table::
   :file: ../table/param_data.csv
   :widths: 25, 25, 10, 10, 30
   :header-rows: 1

Main parameters
================

.. csv-table::
   :file: ../table/param_main.csv
   :widths: 25, 25, 10, 10, 30
   :header-rows: 1

Extra parameters
=================

.. csv-table::
   :file: ../table/param_supp.csv
   :widths: 25, 25, 10, 10, 30
   :header-rows: 1


