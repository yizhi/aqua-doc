##################################
Features
##################################

.. highlight:: matlab

All features are stored in ``fts`` or ``ftsFilter`` fields
of the res structure in output MAT file
This section describes the structure of this field and explains the features.

We assume we are using ``res.fts``. All items listed below are inside it.
For example, when we talk about ``loc.x3D``, we either refer to
``res.fts.loc.x3D`` or ``res.ftsFilter.loc.x3D``.

You can find the same set of items in ``res.ftsFilter``.
Some selected fields will be described in more details.

Most features uses units ``um`` and ``seconds``.
If some features are used to indicator the locations in the movie,
we use the units like ``pixel``, ``voxels`` and ``frames``.

``N``
    Events number

``L``
    Landmark number

``R``
    Region number

``Thr``
    Threshold levels

``H``
    Height of the field of view, in pixels

``W``
    Width of the field of view, in pixels

``T``
    Duration of the movie, in frames


Positions, size, and shape
=================================

Features in this section are related to the size, shape and locations of features.

.. csv-table::
   :file: ../table/res_fts_basic.csv
   :widths: 30,10,20,10,30
   :header-rows: 1

``basic.map``
    The spatial footprint of each event.
    For each event, it is a 2D binary image, with the bright part for events.
    To save space, we crop the event out as patches.

    The purpose of this feature is mainly help the calculation of the
    area and shape based features. It may also be used in visualization.

    The range of cropping is given by::

        rgH = max(min(ih)-1,1):min(max(ih)+1,H);  % range on Y direction
        rgW = max(min(iw)-1,1):min(max(iw)+1,W);  % range on X direction

    H is the Height, W is the width. ``ih`` is the Y direction coordinates
    and ``iw`` is the X direction coordinates of pixels.

``basic.peri``
    Perimeter of the spatial footprint of the event.
    The value is calculated from ``basic.map`` using ``regionprops`` function
    of MATLAB.

``basic.area``
    The area of the spatial footprint. It is also calculated
    from ``basic.map`` using ``regionprops`` function of MATLAB.

``basic.circMetric``
    A score for the circularity of the spatial footprint.
    It is calculated by C^2/4S, where C is the perimeter and S the area,
    as calculated above.
    For shapes that is closer to a circle, the value will be closer to 1.

    .. admonition:: Example

        For cicles, the score is 1. For lines, the score can be very large.

``loc.x3D``
    Exact location of all voxels. Formerly we name it locAbs.    
    Imaging the movie as a 3D volume, then each event can be described 
    by a list of (x,y,t) coordinates. 
    Each element in locAbs contains all coordinates for an event 
    and these coordinates are stored as linear index 
    (see, for example, `here <https://www.mathworks.com/help/
    matlab/math/matrix-indexing.html>`_)

``loc.x2D``
    A list of pixels for the 2D spatial footprint of each event.
    The values are given in linear index as well.

``loc.t0``, ``loc.t1``
    Starting and stopping frames of each event

``bds``
    Each item is a cell. The cell could contain one or multiple arrays.
    Each array describes one outer or inner boundary of the
    spatial footprint (``loc.x2D``) of the event.
    The values are obtained by MATLAB function ``bwboundaries``.

    .. admonition:: Example

        Assume one component is a square, then we have a 4 by 2 array,
        and each row is the Y and X coordinates of the four vertices.


Time and curve related
=================================

Curve related features, like duration and dF/F0
are in field ``res.fts.curve``.
These features are based on the extracted dF/F0 curves.
To compensate the bleaching, remaining motion or other effects,
the trend of the curves are also corrected.
Details on these steps are described in :doc:`/ref/extract`.

.. csv-table::
   :file: ../table/res_fts_curve.csv
   :widths: 30,10,20,10,30
   :header-rows: 1


``rgt1``
    This is the by-product when finding a longer curve for curve related features.
    We only use part the dF/F0 curve to extract curve related features.
    This operation is intended to make the estimation of curve related
    feature more local.
    This feature serves as an outer bound of events duration
    so that we will not under-estimate the event duration.

    .. admonition:: Algorithm:

        Find all events that share spatial footprint with current event
        and occurs earlier than current event.
        Find the one that is temporally closest to current one.
        Find time point ``t0``, which the frame with lowest value of dF/F0
        between these two events.
        Similarly, we can find ``t1`` for events occurring later than current one.
        Then ``rgt1`` is ``t0:t1``.
        If we can not find either ``t0`` or ``t1``,
        we use ``loc.t0`` or ``loc.t1``.


``dffMaxZ``, ``dffMaxPval``
    These two scores describe the significance of the maximum dF/F0.
    It can also be used as a significance measure of the whole event.
    The larger the Z-score, the smaller the p-value, the more significant.

    The Z score is calculated from the dF/F0 curve.
    Let F_max be the maximum value of dF/F0 in one event,
    b_pre be the baseline before F_max and
    b_post be the baseline after F_max,
    then we define Z score as

        Z = min(F_max-b_pre,F_max-b_post)/sigma/2,

    where sigma is the estimated noise standard deviation on
    the dF/F0 curve for current event.

    The p-value is directly calculated from the z score::

        dffMaxPval = 1-normcdf(dffMaxZ)

    Though usually 0.05 is set as the significance threshold,
    for movies with imperfect imaging conditions
    (in terms of noise distribution), which is always the case,
    the significance level should be much lower.

``rise19``, ``fall91``, ``width55``, ``width11``
    We search within the dF/F0 curve inside``rgt1`` to find these four durations.
    We find six time points: 10% onset time, 50% onset time, 90% onset time,
    10% offset time, 50% offset time and 90% offset time.

    .. admonition:: Example

        To find 50% offset time, we find all time points that is
        earlier than the peak and lower than or equal to 50% of the peak intensity.
        Then we get the one that is closet to the peak in time.

    .. Caution::

        The estimates could be inaccurate for multiple reasons.
        First, if the dF/F0 curves are noisy, or contains some residual trend,
        the 10%, 50% and 90% intensity is not correctly estimated.
        The 10% values are especially sensitive. Second, note that each curve
        is the average of the event, which do not consider propagation.
        The time points estimated may not be the one of interest in some scenarios.
        Third, some peaks have very short duration, which requires interpolation to
        get good onset/offset time estimates. We do not have this function yet.

``decayTau``
    The decay time constant tau is obtained by using the MATLAB ``fit`` function
    with a ``exp1`` model. The option for this function is::

        foptions = fitoptions('exp1');
        foptions.MaxIter = 100;

    The curve used for fitting is transformed to make its intensity between 0 and 1.


Single event propagation
=================================

We extract feature for individual event propagation.
To characterize propagation, we binary the movie (at each event) with multiple
thresholds. The intensity thresholds used here are ``minShow1:0.1:0.8``,
where ``minShow1`` is a user specified parameter.

The distances reported in these features are calculated based on the
change of centroid locations of events at different directions along time.
We use the initial centroid of the event as the starting point.

.. admonition:: Example: distance calculation

    The movie has a filed of view of 100 by 100. With a given threshold, the event has
    initially appeared at t=1. We find the initial centroid at coordinates (50,50).
    At t=2, the events grows comparing with t=1, and we want to quantify the distance
    of propagation in the anterior direction from t=1 to t=2. Assume the anterior
    direction is the as the north direction. Then we only look at the event in the
    Y range 50 to 100 and X range 1 to 100. We find the centroid of the event that is
    within that region. If a t=2, the centroid is at coordinates (60,55), then the
    anterior direction propagation distance from t=1 to t=2 is 55-50=5 pixels.
    Usually we transform the units to um when we report the features.

.. admonition:: Definition: growing and shrinking

    These feature records the growing and shrinking behaviro of propagation.
    For example, under a given intensity threshold, assume an event does not arrive a
    location at time t (in other words, the intensity at that location is lower
    than the threshold). Then it arrives at that location at time t+1 (the intensity
    becomes high enough), we say the event grows to that location.
    Later, for example, at time t+2, the signal intensity is lower than the threshold
    again at that location, then we say the event shrinks.
    Therefore, an event can have both growing and shrinking score at each time at a
    given intensity threshold.

.. csv-table::
   :file: ../table/res_fts_prop.csv
   :widths: 30,10,20,10,30
   :header-rows: 1

``propagation.propGrow``
    Each item in the cell for the propagation for one event.
    Assume the event has T0 frames, the item is a T0 by 4 matrix.
    The time range recorded is the same as ``loc.t0`` and ``loc.t1``, or
    ``curve.tBegin`` and ``curve.tEnd``.
    This feature records at each frame, in each of the four direction,
    the distance change comparing with the previous frame.
    As the distance is calculated at multiple thresholds, this feature reports the
    largest one at each frame for each direction.

    .. note::

        We only record positive values in this feature as we are recording growing
        pattern. Negative changes are interpreted as shrinking,
        and are reported in those shrinking features.

``propagation.propGrowOverall``
    Take the summation of all frames in the ``propagation.propGrow`` feature.
    This gives the overall growing type propagation for each event in four directions.

``propagation.propShrink``, ``propagation.propShrinkOverall``
    Similar to ``propagation.propGrow`` and ``propagation.propGrowOverall``,
    except that we record the shrinking distances.

``propagation.areaFrame``
    Each item in the cell array is for one event.
    Assume the event has T0 frames, the item is a T0 by ``Thr`` matrix.
    It record the area of the event at one frame under a certain threshold.

``propagation.areaChange``
    In ``propagation.areaFrame``, by subtracting the value of previous frame from
    current frame, we obtain the event area change at each frame for each threshold.

``propagation.areaChangeRate``
    This is a normalized version of ``propagation.areaChange``. We divide each item in
    ``propagation.areaChange`` with the area of the spatial footprint. The spatial
    footprint contains all spatial locations the event ever visited. The area is given
    by ``basic.area``. We take the **maximum for all thresholds** and report the
    **largest change rate at each frame** in this feature.

``notes.propDirectionOrder``
    Four directions in: anterior, posterior, left lateral and right lateral.
    The user can draw the anterior direction.
    By default, the direction is north (upper).


Region and landmark
=================================

Here we describe region and landmark related features, except those related to
propagation directions. Again, ``L`` is the number of landmarks and ``R`` is the
number of regions. They are either drawn by user or generated from mask builder.
The index of the regions and landmarks are labelled on AQuA GUI. They are also
exported as a PNG file.

.. admonition:: Notes on the 2D maps in features

    In MATLAB, 2D binary images are stored in a 2D array. Assume the image is H by W.
    The top left element of the matrix has coordinate (1,1),
    and the bottom left corner of the matrix has coordinate (1,H).
    On the other hand, the image coordinate system starts from the bottom left, which
    has coordinate (1,1). For consistency, in AQuA, when storing 2D images to matrix,
    we store coordinate (i,j) in image to (H+1-i,j) in matrix. For example,
    (1,1) in image is saved to coordinate (1,H) in the matrix, This makes the matrix
    look the same as the image if we directly display the image in MATLAB. Also, when
    using high level display functions in MATLAB (like ``imshow``), the images is
    correctly shown. The drawback is that the coordinate systems mismatch.
    For example, a large Y coordinate actually point to south, instead of north.
    (for more details, see
    `<https://www.mathworks.com/help/images/image-coordinate-systems.html>`_)

.. csv-table::
   :file: ../table/res_fts_regLmk.csv
   :widths: 40,10,10,10,30
   :header-rows: 1

``region.landMark.mask``
    Each element of the cell is a H by W binary matrix showing the location of the
    landmark. For problems in coordinate system, see notes above.

``region.landMark.center``
    Each row is the center X and Y coordinates for each landmark.

``region.landMark.border``
    Each item is the border pixels for each landmark. It is saved as an array, with
    each row as the X and Y coordinates of a border pixel. It is obtained by ``bwperim``
    function in MATLAB.

``region.landMark.centerBorderAvgDist``
    For each border pixel, we find th distance between the centroid with it.
    We report the average distance in this feature.
    This may be used as a measure for landmark size.

``region.landmarkDist.distPerFrame``
    Each item is a T0 by ``L`` matrix, which is the minimum distance of one
    event to each landmark at different time points. The range of time is from
    ``loc.t0`` to ``loc.t1``.

``region.landmarkDist.distAvg``
    Each item is the **average minimum** distance of an event to each landmark across
    all relevant frames. It is obtained by taking average along time for feature
    ``region.landmarkDist.distPerFrame``.

``region.landmarkDist.distMin``
    Each item is the minimum distance of an event to a landmark across all frames
    when the event exist. It is obtained by taking minimum along time for feature
    ``region.landmarkDist.distPerFrame``.

``region.cell.mask``
    Binary matrix for regions. See also ``region.landMark.mask``

``region.cell.center``
    Center locations for regions. See also ``region.landMark.center``

``region.cell.border``
    Boundary pixels for regions. See also ``region.landMark.border``

``region.cell.centerBorderAvgDist``
    Average distance from centroid to boundaries for regions.
    See also ``region.landMark.centerBorderAvgDist``

``region.cell.incluLmk``
    The inclusion relationship between landmarks and regions.
    If item (i,j) is 1, region i contains landmark j.
    A region do not need to contain any landmark,
    and a landmark can be outside any region.

``region.cell.memberIdx``
    If item (i,j) is 1, event i is (at least partially) included in regions j.
    Here we only consider the spatial footprint.
    Many events are outside any region, so the corresponding row for that event will
    be all zeros. Sometimes regions can overlap, so some events can belong to
    multiple regions. Some regions could contain no events.

``region.cell.dist2border``
    For each event, we use the spatial footprint (``loc.x2D``) to find the minimum
    distance to the border of each region, as long as the event (at least partially)
    inside the region. Otherwise we use ``nan`` as distance.

    .. note::
        We do not consider the movement with respect to regions,
        so we do not record the distance per frame. Use landmark to achieve that goal.

``region.cell.dist2borderNorm``
    We divide values in ``region.cell.dist2border`` by the corresponding
    ``region.cell.centerBorderAvgDist``. This normalizes the distance to border by the
    radius of the region.


Propagation direction (landmark)
=======================================

It is interesting to study whether the events are propagating toward a landmark,
away from a landmark, both, or neither. We save this kind of features in
``res.fts.region.landmarkDir``. The thresholds used in this part is::

    minThr:0.1:0.9;

Here ``minThr`` is a user specified parameter. The frames used are from
``loc.t0`` to ``loc.t1`` for each event.

.. admonition:: Algorithm overview

    For each event, under each intensity threshold, we track the change of spatial
    locations along time. Let `A` be the set of pixels at time `t=0` and `B` the set at
    time `t=1`. The landmark is at location `m`. Assume the event is growing from time 0
    to time 1. We define the new pixels at `t=2` as `C=B\\A`, where `\\` mean removing
    elements (pixels) in A from B.
    We also define the boundary pixels of `A` as `bd(A)`.

    For each pixel in `C`, we find the closest pixel in `bd(A)`.
    The pixel in `C` has a (geodesic) distance `L0` to the landmark, and the pixel in
    `bd(A)` has (geodesic) distance `L1` to the landmark. If `L1` is small than `L0`, the
    pixel implies propagating toward the landmark. Some pixels in `C` has `L1` larger
    than `L0`, which means they imply propagating away from the landmark.

    We can gather all pixels in `C` with `L1` small than `L0` to get the score of
    propagating toward that landmark at this frame. Similarly, we have the score of
    propagating away from the landmark by gathering the pixels in `C` with `L1` larger
    than `L0`. If the pixels in `C` has larger changes between `L0` and `L1`, and/or
    there are a lot of pixels in `C`, we will have larger scores.

    Unlike the features in single event propagation, we do not consider shrinking.
    In other words, if at t=0 the pixels has intensity higher than the threshold, but
    at t=1 the intensity becomes lower than the threshold, we do not consider this as
    propagating toward or away from the landmark.

    Besides, the scores here are not normalized. Therefore, larger events with longer
    durations are more likely to have larger scores. Some ways to compensate this
    are provided in the overlay module of AQuA.

.. admonition:: Notes: units

    The unit for the score of individual pixels is the length (here we use `um`).
    When the events propagates, we gather the score from pixels covering some area.
    Therefore, when we take the summation of the scores among pixels,
    the results can be interpreted as volumes.
    So we use the unit um3. We can also interpret this summation as the
    addition of length, therefore the units can be just um3.

.. admonition:: Notes: disconnected components

    At t=0, the events has several disconnected spatial components
    that do not connect with each other.
    At t=1, the events grows and these components merges. Then how to determine whether
    the events are propagating toward or away from the landmark?
    For each new pixel in`C`, we need to determine the corresponding pixel in `bd(A)`.
    When doing this, we can use the component sizes of `A` as a weight.
    A component with larger size in `A`
    will have a larger weight and becomes more likely to provide the pixel for the
    pixel in `C`. This arrangement is more consistent with visual perception.
    As a extreme case, assume we have two components in t=0, one is very large and one
    is very small, then at t=1, if these two merges, we will have the impression that
    the new grown pixels (`C`) mainly comes from the larger component.
    In the algorithm, if the size a component in `A` is smaller than 20% of `C`,
    we ignore that component when calculating the scores.

.. admonition:: Notes: before and after reaching the landmark

    At t=0, the event has not reached the landmark yet, at t=1, the event covers the
    landmark. Therefore, from t=0 to t=1, the event is both propagating toward and
    away from the landmark. For example, at t=0.5, the frontier of the event has just
    hit the landmark. Then, from t=0.5 to t=1, the frontier continue moves and becomes
    away from landmark. When calculating the score for individual pixel, the algorithm
    first check whether the frontier has ever hit the landmark, which leads to distance 0.
    If it is the case, we will report both propagating toward and away from the landmark.

.. csv-table::
   :file: ../table/res_fts_lmkDir.csv
   :widths: 40,10,20,10,20
   :header-rows: 1

``region.landmarkDir.chgToward``
    The score for each event propagating toward each landmark.

``region.landmarkDir.chgAway``
    The score for each event propagating away from each landmark.

``region.landmarkDir.chgTowardBefReach``
    The score of each event propagating toward each landmark. But we do not
    take into account the scores after reaching the corresponding landmark.
    This feature, along with ``region.landmarkDir.chgAwayAftReach`` is useful
    if we want to distinguish the propagation pattern before and after reaching
    the landmark.

``region.landmarkDir.chgAwayAftReach``
    THe score of each event propagating away from each landmark. But we do not
    take into account the scores before reaching the corresponding landmark.
    This feature, along with ``region.landmarkDir.chgTowardBefReach`` is useful
    if we want to distinguish the propagation pattern before and after reaching
    the landmark.

``region.landmarkDir.pixelToward``
    For each event, there is a H by W by L array. Let
    ``x=res.fts.region.landmarkDir.pixelToward``, then ``x(:,:,l)`` is a 2D array for
    the *l*\ th landmark. The array saves the score of propagating toward that landmark
    for each pixel.

``region.landmarkDir.pixelAway``
    For each event, there is a H by W by L array. Let
    ``x=res.fts.region.landmarkDir.pixelAway``, then ``x(:,:,l)`` is a 2D array for
    the *l*\ th landmark. The array saves the score of propagating away from that
    landmark for each pixel.

``region.landmarkDir.chgTowardThr``
    Similar to ``region.landmarkDir.chgToward``, except that the scores for all
    thresholds are provided.

``region.landmarkDir.chgAwayThr``
    Similar to ``region.landmarkDir.chgAway``, except that the scores for all
    thresholds are provided.


Event groups
=================================

Features related to more than one events are put under ``res.fts.network``.

.. csv-table::
   :file: ../table/res_fts_grp.csv
   :widths: 30,10,20,10,30
   :header-rows: 1

``networkAll.nOccurSameLoc``, ``networkAll.occurSameLocList``
    For event `i`, we find the events whose spatial footprint overlap with it.
    We count the number and save it in element ``(i,1)`` of ``nOccurSameLoc``.
    The list of events are save in item ``(i,1)`` of ``occurSameLocList``.
    Then from the list we find those whose size is not too different
    from event `i` (from 50% to 200%) and save the list in ``(i,2)`` item in
    ``occurSameLocList``.
    We count the number again and save it in element ``(i,2)`` of ``nOccurSameLoc``.
    In all cases, event `i` itself is counted.

``networkAll.nOccurSameTime``, ``networkAll.occurSameTimeList``
    Assume event `i` starts at frame t0 and ends at frame t1, then we find all events
    that exists during t0 and t1. The list is save in ``nOccurSameTime`` and the number
    is saved in item `i` in ``occurSameTimeList``.

``network.nOccurSameLoc``, ``network.occurSameLocList``
    The same as ``networkAll.nOccurSameLoc`` and ``networkAll.occurSameLocList``,
    except that we only use events inside the regions.

``network.nOccurSameTime``, ``network.occurSameTimeList``
    The same as ``networkAll.nOccurSameTime`` and ``networkAll.occurSameTimeList``,
    except that we only use events inside the regions.


Derived features
=================

Some features in the exported file and the overlay function is
derived from the above features. Some variables are defined below.
The description can be found in the overview of outputs (:doc:`/ref/output_file`).

``x``
    The output of the calculation for each feature.

``fts``
    Feature data structure: either ``res.fts`` or ``res.ftsFilter``.

``nEvt``
    Event number.

``xxDi``
    Propagation direction of interest.
    1. Anterior. 2. Posterior. 3. Lateral left. 4. Lateral right

``xxLmk``
    Landmark index of interest.
    The number is the same as those annotated in the overlay.

.. csv-table::
   :file: ../table/feature_derive.csv
   :widths: 40,60
   :header-rows: 1
