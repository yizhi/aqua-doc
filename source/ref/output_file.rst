##########################
Overview of Output
##########################

In this section, we briefly overview the files exported from AQuA.
All output files from AQuA is listed below.

.. csv-table::
   :file: ../table/output.csv
   :widths: 30, 70
   :header-rows: 1

By default, ``name`` is the input data name and ``postfix`` is ``AQuA``.


Main output
============

The main output file in AQUA is ``name``\ _\ ``postfix.mat``.
It contains a MATLAB structure called ``res``. The main contents of it are shown below.
You can use this structure for further analysis.

.. csv-table::
   :file: ../table/res_top_sel.csv
   :widths: 15,15,30,40
   :header-rows: 1

The curves of all events are save in ``res.dffMat`` and ``res.dMat``.
The formal one contains dF/F0 curves and the latter saves original curves
(which is the average of all pixel in this event).

The features of all events are save in MATLAB structure ``res.fts``.
Each category of features is saved in a field of ``res.fts``.
Unless otherwise noted, the units are based on seconds and um.

If you are only interested in filtered and selected events, 
use ``res.dffMatFilter``, ``res.dffFilter``, ``res.ftsFilter`` instead.
The list of selected features are in ``res.evtSelectedList``.

The detection parameters are save in ``res.opts``.
For example, ``res.opts.frameRate`` and ``res.opts.spatialRes`` saves
the spatial and temporal resolution.


Exported features
=============================

We list all exported features and show how they are calculated from basic features
in ``res.fts`` or ``res.ftsFilter``. We also briefly describe them.
More details for basic features can be found in :doc:`/ref/features`.
These features are used in the overlay function of AQuA.
However, in overlay function, the propagtion direction of interest can be selected
in the drop-down menu. Instead, here, all four directions are all listed.
Some features do not have units.

.. csv-table::
   :file: ../table/feature_export.csv
   :widths: 40,10,50
   :header-rows: 1

In either ``name``\ _\ ``postfix``\ .xlsx or ``name``\ _\ ``postfix``\ _region_\ ``i`` .xlsx,
the same set of features are reported.

