##########################
Event Detection Pipeline
##########################

This section describes the major components of the pipeline and
briefly introduced the algorithms used.
A rough understanding of the pipeline will help choose parameters to get
good detection results.

Overview
=========


Foreground detection
=====================


Super voxels
=============



Super events
=============




Events
=======







