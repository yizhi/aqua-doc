##########################
A Guided Tour
##########################

Get data ready
===============


Open file and set imaging parameters
=====================================



Add landmarks and regions
==========================



Run detection and check results
================================


View events and curves
=======================


Proof read results
===================



Export
=======



