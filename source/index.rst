#############################################
AQuA: Astrocyte Quantification and Analysis
#############################################

**Welcome**

The step by step user guide can be downloaded `here <https://1drv.ms/b/s!AmZJxr5-ArTyh7Ea6pkUcC9b5oMirQ>`_.
We are in the process of migrating the documents to Read the Docs.

**Get AQUA**

Visit `AQuA on GitLab <https://gitlab.com/yizhi/aqua>`_

..
    **What to do next?**

    **Contact**

    Yizhi Wang. Email: yzwang@vt.edu

    **Cite**

    Draft in preparation.

.. toctree::
    :maxdepth: 3
    :caption: Contents

    idxx/out
    idxx/param

